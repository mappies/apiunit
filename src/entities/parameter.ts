import * as fs from 'fs';

import { IParameter } from '../interfaces/IParameter';

export class Parameter implements IParameter {
    parameters: {[key: string]: string | object};

    constructor(paramFilePath?: string){
        this.parameters = paramFilePath ? JSON.parse(fs.readFileSync(paramFilePath, 'utf-8')) : {}
    }

    setParam(paramName: string, value: string) {
        this.parameters[paramName] = value;
    }

    getScenarioParams(scenarioName: string): IParameter {
        if(this.parameters.settings 
            && this.parameters.settings['scenarios'] 
            && this.parameters.settings['scenarios'][scenarioName]){
                let newParam = new Parameter();
                newParam.parameters = {...this.parameters, ...this.parameters.settings['scenarios'][scenarioName]};
                return newParam;
        }
    }

    getScenarioGroups(scenarioName: string): string[] {
        if(this.parameters.settings
            && this.parameters.settings['groups']
            && this.parameters.settings['groups'][scenarioName]){
            return this.parameters.settings['groups'][scenarioName]
        }
        return [];
    }
}