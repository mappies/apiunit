import * as fs from 'fs';

import { IActualResponse } from '../interfaces/iActualResponse';
import { IRequest } from '../interfaces/iRequest';
import { ITestCase } from '../interfaces/iTestCase';
import { injectDynamicParameters, injectParameters } from './helpers';

export abstract class Request implements IRequest
{
    testCase:ITestCase;
    requestPath:string;
    
    get shouldRetried():number
    {
        return 0;
    }

    get rawRequest():string
    {
        let result = fs.readFileSync(this.requestPath, 'utf8');
        
        result = injectParameters(result, this.testCase.suite.parameters);
        result = injectDynamicParameters(this.testCase.suite.testCases, result);

        return result;
    }

    constructor(requestPath:string)
    {
        this.requestPath = requestPath;
    }

    abstract execute(verbose?:boolean): Promise<IActualResponse>;
};