import * as path from 'path';

import { IActualResponse } from '../interfaces/iActualResponse';
import { IDiff } from '../interfaces/iDiff';
import { IExpectedResponse } from '../interfaces/iExpectedResponse';
import { IRequest } from '../interfaces/iRequest';
import { IResult } from '../interfaces/iResult';
import { ITestCase } from '../interfaces/iTestCase';
import { ITestSuite } from '../interfaces/iTestSuite';
import { Diff } from './diff';
import { Result } from './result';

function sleep(ms:number): Promise<void>
{
    return new Promise((resolve, reject)=>setTimeout(resolve, ms));
}

export class TestCase implements ITestCase
{
    diff:IDiff[];
    suite:ITestSuite;
    get requestFilePath():string
    {
        return this.request.requestPath;
    }
    actualResponse:IActualResponse;
    
    get expectedResponseFilePath():string
    {
        return this.expectedResponse.responsePath;
    }

    request:IRequest;
    expectedResponse:IExpectedResponse;

    get name():string
    {
        return path.basename(this.requestFilePath, path.extname(this.requestFilePath));
    }

    constructor(request:IRequest, expectedResponse:IExpectedResponse)
    {
        this.request = request;
        this.expectedResponse = expectedResponse;

        this.request.testCase = this;
        this.expectedResponse.testCase = this;
    }

    async execute(verbose?:boolean): Promise<IResult>
    {
        try
        {
            let startTime = new Date().getTime();

            do
            {   
                if(this.diff) {
                    if(verbose)
                    {
                        console.log(`Retry... (${this.request.shouldRetried})`)
                    }
                    await sleep(500);
                }
                this.actualResponse = await this.request.execute(verbose);
                
                this.diff = this.actualResponse.diff(this.expectedResponse);

            } while(this.diff.length !== 0 && this.request.shouldRetried && (new Date().getTime() - startTime < this.request.shouldRetried));
        }
        catch(error)
        {
            this.diff = [new Diff(error)];
        }

        return new Result(this.name, this.diff);
    }
};