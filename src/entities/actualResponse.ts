import { IActualResponse } from '../interfaces/iActualResponse';
import { IDiff } from '../interfaces/iDiff';
import { IExpectedResponse } from '../interfaces/iExpectedResponse';
import { Diff } from './diff';
import { parse as parseXml } from 'fast-xml-parser';
import { gunzipSync } from 'zlib';
import * as jsonPath from 'jsonpath'

let deepDiff = require('deep-diff').diff;

export class ActualResponse implements IActualResponse
{
    statusCode:number;
    headers:{ [key: string]: any };
    body:any;

    constructor(statusCode:number, headers:object, body:any)
    {
        this.statusCode = statusCode;
        this.headers = headers;

        for(let key in this.headers)
        {
            let value = this.headers[key];
            delete this.headers[key];
            this.headers[key.toLowerCase()] = value;
        }

        this.body = body;
    }

    private getStringValue(obj: any): string {
        if(typeof obj === 'string' || obj === undefined){
            return obj;
        }

        return JSON.stringify(obj);
    }

    private getObjectValue(obj: any): object {
        if(typeof obj === 'string' && obj !== ''){
            return JSON.parse(obj);
        }
        return obj;
    }

    diff(expectedResponse:IExpectedResponse): IDiff[]
    {
        let diff:IDiff[] = [];

        if(expectedResponse.statusCode !== this.statusCode)
        {
            diff.push(new Diff(`Status code assertion failed.  Expected '${expectedResponse.statusCode}' to equal to '${this.statusCode}'.`));
        }

        let jsonArraySortOrdering: string;

        for(let expectedHeader in expectedResponse.headers)
        {

            let loweredExpectedHeader = expectedHeader.toLowerCase();

            if (loweredExpectedHeader === 'apiunit-ignore-array-order') {
                jsonArraySortOrdering = expectedResponse.headers[loweredExpectedHeader];
                if (jsonArraySortOrdering === "true") {
                    jsonArraySortOrdering = "$..*"
                }
                continue;
            }

            if(!(loweredExpectedHeader in this.headers))
            {
                diff.push(new Diff(`Header assertion failed. Expected header '${expectedHeader}' is missing from the response.`));
            }
            else if(new RegExp(`^${expectedResponse.headers[expectedHeader]}$`).test(this.headers[loweredExpectedHeader]))
            {
                continue;
            }
            else if(expectedResponse.headers[expectedHeader] !== this.headers[loweredExpectedHeader])
            {
                diff.push(new Diff(`Header assertion failed. Expected '${expectedResponse.headers[expectedHeader]}' to equal to '${this.headers[loweredExpectedHeader]}'`));
            }
        }

        if ('content-type' in this.headers && this.headers['content-type'] === 'application/gzip')
        {
            try
            {
                this.body = gunzipSync(this.body).toString();
            }
            catch (error)
            {
                diff.push(new Diff("Uable to gunzip response: ".concat(error)));
            }

            if(expectedResponse.body !== this.body)
            {
                diff.push(new Diff(`Body assertion failed. Expected '${this.getStringValue(expectedResponse.body)}' to equal to '${this.getStringValue(this.body)}'`));
            }
        }
        else if(this.headers?.['content-type']?.startsWith('application/json'))
        {
            this.body = this.body?.toString();
            let jsonDiff = this.diffJson(this.getObjectValue(this.body), this.getObjectValue(expectedResponse.body), jsonArraySortOrdering)
            if(jsonDiff) diff = diff.concat(jsonDiff);
        }
        else if('content-type' in this.headers && /^(text|application)\/xml/.test(this.headers['content-type'] as any))
        {
            this.body = this.body?.toString();
            let jsonDiff = this.diffJson(parseXml(this.body), parseXml(expectedResponse.body), jsonArraySortOrdering);
            if(jsonDiff) diff = diff.concat(jsonDiff);
        }
        else
        {
            this.body = this.body?.toString();
            if (expectedResponse.body !== this.body)
            {
                diff.push(new Diff(`Body assertion failed. Expected '${this.getStringValue(expectedResponse.body)}' to equal to '${this.getStringValue(this.body)}'`));
            }
        }

        return diff;
    }

    private diffJson(json:any, expectedJson:any, arrayOrderPathExpressions: string): Diff[]
    {
        let diff:Diff[] = [];

        const paths = this.getPathsFromExpectedJsonAndPathExpressions(expectedJson, arrayOrderPathExpressions);

        let differences = deepDiff(json,expectedJson, { normalize: normalize(paths) });

        if(!differences) return;

        for(let difference of differences)
        {
            let value = difference.lhs;
            let expectedValue = difference.rhs;

            if(difference.kind === 'A')
            {
                value = difference.item.lhs;
                expectedValue = difference.item.rhs;

                if(expectedValue === undefined)
                {
                    diff.push(new Diff(`Body assertion failed at '$.${difference.path}[${difference.index+1}]'. An array item is missing. '${this.getStringValue(value)}' does not exist in the response.`));
                }
                else if(value === undefined)
                {
                    diff.push(new Diff(`Body assertion failed at '$.${difference.path}[${difference.index+1}]'. An array item is missing. Expected '${this.getStringValue(expectedValue)}' to equal to '${this.getStringValue(value)}'`));

                }
                else
                {
                    diff.push(new Diff(`Body assertion failed at '$.${difference.path}[${difference.index+1}]'. Arrays items do not matched.  Expected '${this.getStringValue(expectedValue)}' to equal to '${this.getStringValue(value)}'`));
                }
            }
            else if(difference.kind === 'N')
            {
                diff.push(new Diff(`Body assertion failed at '$.${difference.path}'. Expected '${this.getStringValue(expectedValue)}' but it is missing.'`));
            }
            else if(!new RegExp(`^${expectedValue}$`).test(value))
            {
                diff.push(new Diff(`Body assertion failed at '$.${difference.path}'. Expected '${this.getStringValue(expectedValue)}' to equal to '${this.getStringValue(value)}'`));
            }
        }

        return diff;
    }

    /**
     * Returns a list of data paths processed from expectedJson and arrayOrderPathExpressions
     * See https://jsonpath.com/ for interactive example of how paths are generated based on the supplied expressions and expectedJson data
     * Example:
     * ```
     *  expectedJson = {
     *      "data": [
     *          {
     *              "key1": "a"
     *          },
     *          {
     *              "item1": ["a","b","c"],
     *              "item2": ["d","e","f"]
     *          }
     *      ]
     * }
     * ```
     * 
     * `arrayOrderPathExpressions = "$.data"` would result in 
     * ```
     * paths = [
     *     ["data"],
     * ]
     * ```
     * 
     * `arrayOrderPathExpressions = "$.data[*].item1"` would result in 
     * ```
     * paths = [
     *     ["data", 1, "item1"]
     * ]
     * ```
     * 
     * and `arrayOrderPathExpressions = "$.data,$.data[*].item1"` would result in 
     * ```
     * paths = [
     *     ["data"],
     *     ["data", 1, "item1"]
     * ]
     * ```
     */
     private getPathsFromExpectedJsonAndPathExpressions(expectedJson:any, arrayOrderPathExpressions: string): jsonPath.PathComponent[][]
     {
         if (!arrayOrderPathExpressions) {
             return undefined
         }
 
         const paths: jsonPath.PathComponent[][] = [];
         for (const pathExpression of arrayOrderPathExpressions.split(','))
         {
             paths.push(...jsonPath.paths(expectedJson, pathExpression).map(p => p.slice(1)));;
         }
         return paths
     }
};

/**
 * Takes expected paths as an arguments and returns the normalize function to be used by deep-diff. See Deep Diff documentation at https://github.com/flitbit/diff#readme for more information about the expected normalize function.
 */
function normalize(expectedPaths: jsonPath.PathComponent[][])
{
    if (!expectedPaths) return;

    /**
     * Refer to deep diff's normalize() documentation at https://github.com/flitbit/diff#readme for more details
     */
    return function <L,R>(path: string[], key: string, lhs:L, rhs:R): [L,R]|undefined
    {
        if (!Array.isArray(lhs) || !Array.isArray(rhs)) {
            return;
        }

        // lhs and rhs are swapped here because rhs is the expected value and lhs is actual
        const data = replaceAndRearrangeIdenticalItems([...path, key], rhs, lhs, expectedPaths);
        if (data) {
            return [data[1], data[0]];
        }
    }
}

// A custom deep compare is used instead of deep diff since we need to maintain knowledge of the original path in order to support unordered arrays for only some nested items
function deepCompare(a: unknown, b: unknown, path: (string|number)[], unorderedPaths: jsonPath.PathComponent[][])
{
    if (typeof b === "string")
    {
        try {
            return  a === b || new RegExp(`^${b}$`).test(a as any)
        } catch {
            // we can safely return false here because an error is only thrown when 'b' is not a valid regular expression or 'a' cannot be implicitly cast to a string
            return false;
        }
    }

    if ((!a && !!b) || (!b && !!a)) {
        return false;
    }

    if (typeof a !== typeof b) {
        return false;
    }

    if (typeof a === "number" || typeof a === "boolean")
    {
        return a === b;
    }

    // Do a deep unordered compare (rearrange and replace) for a and b only when they are both arrays and their path is one of the paths in the the unorderedPaths list
    if (Array.isArray(a)
        && Array.isArray(b)
        && unorderedPaths.some(p => p.every((segment, i) => segment === path[i])))
    {

        const result = replaceAndRearrangeIdenticalItems(
            path,
            jsonDeepCopy(b),
            jsonDeepCopy(a),
            unorderedPaths
        );

        if (!result) return false;

        return result[0].every(value => value === unorderedArrayCompareFlag);
    }

    return Object.entries(a).every(([key,value]) => deepCompare(value, b[key], [...path, key], unorderedPaths));
}

const unorderedArrayCompareFlag = Symbol();
/**
 * Replaces identical items with a flag, and rearranges the right array (rh) to place identical values in the same spot they're in in the left array (lhs)
 */
function replaceAndRearrangeIdenticalItems<L extends unknown[],R extends unknown[]>(path: (string|number)[], lhs: L, rhs: R, unorderedPaths: jsonPath.PathComponent[][]): [L,R]
{
    for (const expectedPath of unorderedPaths) {
        // check path is strictly equal (at the same depth and has identical segments) so that we're not allowing any arrays other than those specified by the user to be unordered
        if (path.length === expectedPath.length && path.every((segment,i) => segment === expectedPath[i]))
        {
            for (let i = 0; i < lhs.length; i++) {
                const j = rhs.findIndex(value => deepCompare(value, lhs[i], [...path, i], unorderedPaths));
                if (j > -1) {
                    lhs[i] = unorderedArrayCompareFlag;
                    rhs[j] = rhs[i];
                    rhs[i] = unorderedArrayCompareFlag;
                }
            }
            return [lhs,rhs];
        }
    }
}

const jsonDeepCopy = <T extends any>(data: T): T => JSON.parse(JSON.stringify(data));
