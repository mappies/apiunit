import { IRequest as IHttpRequest, Request as HttpRequest } from 'http-file';
import * as Consumer from 'sqs-consumer';
import * as Producer from 'sqs-producer';
import { v4 as uuid } from 'uuid';
import { IActualResponse } from '../../interfaces/iActualResponse';
import { ActualResponse } from '../actualResponse';
import { Request } from '../request';

const AWS = require('aws-sdk');


const FIFO_POSTFIX = ".fifo";
export class MsgTestRequest extends Request
{

    get shouldRetried():number
    {
        let retryHeaders = this.request.headers && Object.keys(this.request.headers).filter(h => h.toUpperCase() === 'APIUNIT-RETRY').map(h => this.request.headers[h]);

        if(!retryHeaders || retryHeaders.length === 0) return 0;

        if(retryHeaders[0] === 'true') return 15000;
        
        let waitFor = Number(retryHeaders[0]);

        if(Number.isNaN(waitFor)) return 0;

        return waitFor;
    }

    private _responseQueue:string;
    get responseQueue():string
    {
        return this._responseQueue || (this._responseQueue = this.request.headers['response']);
    }

    private _request:IHttpRequest;
    get request():IHttpRequest
    {
        return this._request || (this._request = HttpRequest.parse(this.rawRequest));
    }

    private _awsRegion:string;
    get awsRegion():string
    {
        return this._awsRegion || (this._awsRegion = this.request.headers['aws-region'] || process.env.AWS_REGION || 'us-east-1');
    }

    constructor(file:string)
    {
        super(file);
    }

    async execute(verbose?:boolean): Promise<IActualResponse>
    {
        let response;
        if(this.request.method === 'PUSH')
        {
            //Clear the queue
            let msg = undefined;
            while((msg = await this.read(this.responseQueue, 2000)) !== undefined)
            {
                if(verbose){ console.log('Discard: ', JSON.stringify(msg)); }
            }

            //Send a message
            if(verbose) { console.log('Sending: ', JSON.stringify(this.request)); }
            await this.send(this.request);

            //Wait for a response message.
            response = await this.read(this.responseQueue)
            try
            {
                response = JSON.parse(response)['payload'];
            }
            catch {}
            
            if(verbose){ console.log('Receiving: ', JSON.stringify(response));}
        }
        else if(this.request.method === 'PULL')
        {
            //Read a message
            response = await this.read(this.request.path);
            let headers = {}
            try
            {
                response = JSON.parse(response);
                headers['Content-Type'] = 'application/json';
            }
            catch {}

            if(verbose){ console.log('Receiving: ', JSON.stringify(response));}
            if (!response.statusCode || !response.headers || !response.body)
            {
                response = { statusCode: 0, headers, body: response }
            }
        }
        else
        {
            throw new Error(`Unrecognized method '${this.request.method}'.  Expected either PUSH or PULL`)
        }
        
        return new ActualResponse(response.statusCode, response.headers, response.body ? JSON.stringify(response.body) : '');
    }

    private send(request:IHttpRequest): Promise<void>
    {
        var producer = Producer.create({ queueUrl: request.path, region: this.awsRegion });
        
        return new Promise((resolve, reject) => 
        {
            let data = {
                id: uuid(),
                body: request.body,
                groupId: request.path.endsWith(FIFO_POSTFIX) ? uuid() : undefined,
                deduplicationId: request.path.endsWith(FIFO_POSTFIX) ? uuid() : undefined,
            };

            producer.send([data], (error) => {
              if (error) return reject(error);
              return resolve();
            });
        });
    }

    private read(queue:string, timeoutInMilliseconds:number = 10000): Promise<any>
    {
        return new Promise((resolve, reject) =>{
            let consumer;
            let result;
            let error;
            let running;
            let timer = undefined;
            
            let stop = () => {
                clearTimeout(timer);
                if(running) consumer.stop();
                running = false;
            };

            try
            {
                consumer = Consumer.create({
                                                queueUrl: queue,
                                                region: this.awsRegion,
                                                handleMessage: (message, done) => 
                                                {
                                                    result = message.Body;
                                                    done();
                                                    stop();
                                                },
                                                batchSize: 1,
                                                waitTimeSeconds: timeoutInMilliseconds/1000,
                                                sqs: new AWS.SQS({region:this.awsRegion})
                                            });
                consumer.on('error', (err) => 
                {
                    error = err;
                    stop();
                });

                consumer.on('stopped', ()=>
                {
                    running = false;
                    return error ? reject(error) : resolve(result);
                });

                running = true;
                consumer.start();

                timer = setTimeout(stop, timeoutInMilliseconds);
            }
            catch(err)
            {
                error = err;
                stop();
            }
        });
    }
};
