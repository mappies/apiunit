import { IRequest } from './iRequest';

export interface IRequestRepository
{
    get(): IRequest[];
};