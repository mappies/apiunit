import { ITestCase } from './iTestCase';

export interface IExpectedResponse
{
    readonly statusCode:number
    readonly headers:object;
    readonly body:any;
    responsePath:string;
    testCase:ITestCase;
};