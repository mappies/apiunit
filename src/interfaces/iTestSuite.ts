import { EventEmitter } from 'events';
import { Dictionary } from 'typescript-collections';

import { IResult } from './iResult';
import { ITestCase } from './iTestCase';

export interface ITestSuite extends EventEmitter
{
    parameters:object;
    testCases:Dictionary<string, ITestCase>;
    
    execute(verbose:boolean): Promise<IResult[]>;
};