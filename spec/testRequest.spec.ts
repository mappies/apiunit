import { assert } from 'chai';
import * as TypeMoq from 'typemoq';
import { Dictionary } from 'typescript-collections';

import { ActualResponse } from '../src/entities/actualResponse';
import { Request } from '../src/entities/request';
import { TestCase } from '../src/entities/testCase';
import { TestSuite } from '../src/entities/testSuite';


class FakeTestRequest extends Request
{
    constructor(path:string)
    {
        super(path);
    }

    async execute(): Promise<ActualResponse> {
        throw new Error("Method not implemented.");
    }
};

class FakeActualResponse extends ActualResponse
{
    constructor(body: any, statusCode: number = 200, headers:{} = {})
    {
        super(statusCode, headers, body);
    }
};

describe('TestRequest', () => 
{ 
    it('Load a test request properly', async () => 
    {
        let testSuite = TypeMoq.Mock.ofType<TestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({'PARAM1': 'VALUE1'}));
        testSuite.setup((s)=>s.testCases).returns(()=>new Dictionary<string, TestCase>());

        let testCase = TypeMoq.Mock.ofType<TestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);


        let req = new FakeTestRequest('./assets/testRequest/api/request.req');
        req.testCase = testCase.object;

        assert.equal(req.rawRequest, "Hi there");
    });

    it('Inject static parameters properly', async () => 
    {
        let testSuite = TypeMoq.Mock.ofType<TestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({'PARAM1': 'VALUE1', 'PARAM3': 'VALUE3'}));
        testSuite.setup((s)=>s.testCases).returns(()=>new Dictionary<string, TestCase>());

        let testCase = TypeMoq.Mock.ofType<TestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);

        let req = new FakeTestRequest('./assets/testRequest/api/request_with_params.req');
        req.testCase = testCase.object;

        assert.equal(req.rawRequest, "VALUE1\n\n${PARAM2}\n\nVALUE3");
    });
    
    it('Inject dynamic parameters properly', async () => 
    {
        let testCase1 = TypeMoq.Mock.ofType<TestCase>();
        testCase1.setup((c)=>c.actualResponse).returns(()=>new FakeActualResponse('{"property1": "Hi there", "property2": 42 }'));

        let testCases = new Dictionary<string, TestCase>();
        testCases.setValue('test-1', testCase1.object);

        let testSuite = TypeMoq.Mock.ofType<TestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({}));
        testSuite.setup((s)=>s.testCases).returns(()=>testCases);

        let testCase = TypeMoq.Mock.ofType<TestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);

        let req = new FakeTestRequest('./assets/testRequest/api/request_with_dynamic_params.req');
        req.testCase = testCase.object;

        assert.equal(req.rawRequest, "Hi there 42\nand undefined ${test-2:/property1}");
    });
    
    it('Inject dynamic parameters - handle text/xml response properly', async () => 
    {
        let testCase1 = TypeMoq.Mock.ofType<TestCase>();
        testCase1.setup((c)=>c.actualResponse).returns(()=>new FakeActualResponse('<property1>Hi there</property1><property2>42</property2>', undefined, {'content-type': 'text/xml'}));

        let testCases = new Dictionary<string, TestCase>();
        testCases.setValue('test-1', testCase1.object);

        let testSuite = TypeMoq.Mock.ofType<TestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({}));
        testSuite.setup((s)=>s.testCases).returns(()=>testCases);

        let testCase = TypeMoq.Mock.ofType<TestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);

        let req = new FakeTestRequest('./assets/testRequest/api/request_with_dynamic_params.req');
        req.testCase = testCase.object;

        assert.equal(req.rawRequest, "Hi there 42\nand undefined ${test-2:/property1}");
    });
    
    it('Inject dynamic parameters - handle application/xml response properly', async () => 
    {
        let testCase1 = TypeMoq.Mock.ofType<TestCase>();
        testCase1.setup((c)=>c.actualResponse).returns(()=>new FakeActualResponse('<property1>Hi there</property1><property2>42</property2>', undefined, {'content-type': 'application/xml'}));

        let testCases = new Dictionary<string, TestCase>();
        testCases.setValue('test-1', testCase1.object);

        let testSuite = TypeMoq.Mock.ofType<TestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({}));
        testSuite.setup((s)=>s.testCases).returns(()=>testCases);

        let testCase = TypeMoq.Mock.ofType<TestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);

        let req = new FakeTestRequest('./assets/testRequest/api/request_with_dynamic_params.req');
        req.testCase = testCase.object;

        assert.equal(req.rawRequest, "Hi there 42\nand undefined ${test-2:/property1}");
    });

    it('Inject dynamic parameters - handle non-json response properly', async () => 
    {
        let testCase1 = TypeMoq.Mock.ofType<TestCase>();
        testCase1.setup((c)=>c.actualResponse).returns(()=>new FakeActualResponse('This is not a json file.'));

        let testCases = new Dictionary<string, TestCase>();
        testCases.setValue('test-1', testCase1.object);

        let testSuite = TypeMoq.Mock.ofType<TestSuite>();
        testSuite.setup((s)=>s.parameters).returns(()=>({}));
        testSuite.setup((s)=>s.testCases).returns(()=>testCases);

        let testCase = TypeMoq.Mock.ofType<TestCase>();
        testCase.setup((c)=>c.suite).returns(()=>testSuite.object);

        let req = new FakeTestRequest('./assets/testRequest/api/request_with_dynamic_params_and_non_json_response.req');
        req.testCase = testCase.object;

        assert.equal(req.rawRequest, "${test-1:/property1}");
    });
});
