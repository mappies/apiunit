import { assert } from 'chai';

import { TestScenario } from '../src/entities/testScenario';

describe('Test Group Parameters', async () => 
{ 
    function setup(sourceDir: string): TestScenario 
    {
        let scenario = new TestScenario(sourceDir,'assets/parameters3.json', 'requests','responses', '',[],'echo "echo echo echo!"');
        assert.equal(scenario.paramsFile, 'assets/parameters3.json');
        assert.equal(scenario.requestDirName, 'requests');
        assert.equal(scenario.responseDirName, 'responses');
        assert.deepEqual(scenario.scenarios, []);
        assert.equal(scenario.filePrefix, '');
        assert.equal(scenario.beforeEachCommand, 'echo "echo echo echo!"');
        assert.equal(scenario.testSuites.size(), 0);
        return scenario;
    }

    it('Create a default scenario', async () => 
    {
        let scenario = setup('assets/testSuite');
        await scenario.setup();
        assert.deepEqual(scenario.testsToRun(), ['assets/testSuite-scn1', 'assets/testSuite-scn2']);
        assert.deepEqual(scenario.testSuites.getValue('assets/testSuite-scn1').parameters,{
            "PARAM1":"scn1",
            "PARAM2":"scn1",
            "settings":{
                "scenarios":{
                    "scn1": {
                        "PARAM1":"scn1",
                        "PARAM2":"scn1"
                    },
                    "scn2": {
                        "PARAM1":"scn2",
                        "PARAM2":"scn2"
                    }
                },
                "groups":{
                    "assets/testSuite":["scn1","scn2"]
                }
            }
        })
        assert.deepEqual(scenario.testSuites.getValue('assets/testSuite-scn2').parameters,{
            "PARAM1":"scn2",
            "PARAM2":"scn2",
            "settings":{
                "scenarios":{
                    "scn1": {
                        "PARAM1":"scn1",
                        "PARAM2":"scn1"
                    },
                    "scn2": {
                        "PARAM1":"scn2",
                        "PARAM2":"scn2"
                    }
                },
                "groups":{
                    "assets/testSuite":["scn1","scn2"]
                }
            }
        })
        assert.equal(scenario.testSuites.size(), 2);
        assert.equal(scenario.testsToRun().length, 2);
    });
});